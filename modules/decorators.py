#!flask/bin/python
"""
Api_auth
==================
This module provides Basic API authentication for Flask routes.
:copyright: (C) 2016 by Brian Paintsil.
:license:   BSD, see LICENSE for more details.
"""

import uuid
from flask import session, flash, redirect, url_for
from flask import request
from flask import Response
from functools import wraps
from modules import dbFunctions



class Validates():
  """docstring for APIAuth"""
  # def __init__(self, arg):
  #   super(APIAuth, self).__init__()
  #   self.arg = arg

  def verify_token(self, access_token):
    """This function is called to check if access_token combination is valid."""
    # fetch from the database and compare
    db = dbFunctions.db_class()
    condition = "WHERE api_key='{0}'".format(access_token)
    token = db.select_from_table("tbl_institution", "*", condition)
    if len(token) == 1:
      token = token[0]
      for k in token:
        token['api_key'] = str(token['api_key'])
      print("==Token success==")
      return access_token == token['api_key']
    else:
      print("==Invalid Token==")
      return ''


  def authenticate(self):
      """Sends a 401 response that enables basic API auth"""
      return Response(
      'Could not verify your access level for that URL.\n'
      'You have to login with proper credentials', 401,
      {'WWW-Authenticate': 'Basic realm="Authentication Required"'})

  # API Authentication decorator
  # To handle API ACCESS TOKENS
  def requires_auth(self, f):
    @wraps(f)
    def wrapper(*args, **kwds):
      print("Calling require_auth function...")
      auth = request.headers.get('Authorization')
      if not auth or not self.verify_token(auth):
        return self.authenticate()
      return f(*args, **kwds)
    return wrapper

  # Web Application login decorator
  # To handle whether or not a user
  # has 'logged_in' in their session
  def login_required(self, f):
    @wraps(f)
    def wrap(*args, **kwargs):
      if 'logged_in' in session:
        return f(*args, **kwargs)
      else:
        flash("You need to login first")
        return redirect(url_for('search'))
    return wrap

  def generate_keypair(fqdn='nsano.com'):
    pri = self.generate_private_key(fqdn)
    pub = self.generate_public_key(fqdn)

    db = dbFunctions()


  def generate_private_key(fqdn):
    key = str(uuid.uuid5(uuid.NAMESPACE_DNS, fqdn))
    return key.replace('-', '')


  def generate_public_key(fqdn):
    key = str(uuid.uuid5(uuid.NAMESPACE_DNS, fqdn))
    return key.replace('-', '')[0:15]
