import os
import sys
import json
import requests
import datetime

from passlib.hash import sha256_crypt
from modules import dbFunctions
from threading import Thread



############################
###     UTILITIES        ###
############################

class utility(object):
  """
  Utility class that contains various utility
  methods to easily assist with validations
  many reference ID generation
  """
  def handle_msisdn(msisdn):
    m = str(msisdn)
    for x in range(len(m)):
        if x == 0:
            if m[x] == "0":
                return "233"+str(m[1:])
            else:
                return m


############################
###    DATABASE CALLS    ###
############################

class app_calls(object):
  """docstring for app_calls"""
  def login(self, username):
    '''
    Function to handle login interactions with
    the database.
    Params: [username, password]
    Return: List of dictionaries
    '''
    db = dbFunctions.db_class()
    table = "tbl_users"
    condition = "WHERE username='{0}'".format(username)
    sel = db.select_from_table(table, ["*"], condition)
    if sel:
        return sel[0]
    return False



  def logout(self, username):
    '''
    Function to handle logout interaction with
    the database
    Params: [username]
    Return: Bool
    '''
    pass


  def create_admin(self, username, firstname, lastname, password):
    '''
    Function to handle creation of administrator
    in the database
    Params: [username, firstname, lastname, password]
    Return: Bool
    '''
    #create a hash for the password
    password = sha256_crypt.encrypt(password)
    #save new admin details to the database.
    db = dbFunctions.db_class()
    table = "tbl_users"
    fields = {"username": username, "firstname": firstname, "lastname": lastname, "password": password }
    try:
      res = db.insert_in_table(table, fields)
      if res:
        return True
      return False
    except Exception as e:
      print(e)
      return False


  def load_admin(self):
    '''
    Function to fetch all admin from the database
    Params: [void]
    return [List]
    '''
    db = dbFunctions.db_class()
    table = "tbl_users"
    try:
      res = db.select_from_table(table)
      print(res)
      if res:
        for r in res:
          r['created_at'] = str(r['created_at'])
        print(res)
        return res
      else:
        return False
    except Exception as e:
      print(e)
      return False


  def create_employee(self, firstname, lastname, department, email):
    '''
    Function to handle creation of employee
    in the database
    Params: [username, firstname, lastname, password]
    Return: Bool
    '''

    db = dbFunctions.db_class()
    table = "tbl_employees"
    fields = {"firstname": firstname, "lastname": lastname, "department": department, "email": email }
    try:
      res = db.insert_in_table(table, fields)
      if res:
        return True
      return False
    except Exception as e:
      print(e)
      return False


  def load_employees(self):
    '''
    Function to fetch all employees from the database
    Params: [void]
    return [List]
    '''
    db = dbFunctions.db_class()
    table = "tbl_employees"
    try:
      res = db.select_from_table(table)
      print(res)
      if res:
        for r in res:
          r['created_at'] = str(r['created_at'])
        print(res)
        return res
      else:
        return False
    except Exception as e:
      print(e)
      return False

  def fetch_employee(self, firstname):
    '''
    Function to fetch a specific employee from the database
    Params: [firstname]
    return [List]
    '''
    db = dbFunctions.db_class()
    table = "tbl_employees"
    try:
      res = db.select_from_table(table, ["*"], "WHERE firstname='{0}'".format(firstname))
      # print(res)
      if res:
        for r in res:
          r['created_at'] = str(r['created_at'])
        # print(res)
        return res
      else:
        return False
    except Exception as e:
      raise e
      return False

  def download_reports(self, from_date, to_date, report_type):
    '''
    Function to export reports
    Params: [from_date, to_date]
    return [List]
    '''
    db = dbFunctions.db_class()
    if report_type == "logs":
      try:
        condition = "WHERE created_at BETWEEN '{0}' AND '{1}'".format(from_date, to_date)
        res = db.select_from_table("tbl_logs", ["*"], condition)
        if res:
          for r in res:
            r['created_at'] = str(r['created_at'])
          print(res)
          return res
        else:
          return False
      except Exception as e:
        print(e)
        return False
    elif report_type == "employees":
      try:
        condition = "WHERE created_at BETWEEN '{0}' AND '{1}'".format(from_date, to_date)
        res = db.select_from_table("tbl_employees", ["firstname", "lastname", "department", "email", "created_at"], condition)
        if res:
          for r in res:
            r['created_at'] = str(r['created_at'])
          print(res)
          return res
        else:
          return False
      except Exception as e:
        print(e)
        return False
    else:
      try:
        condition = "WHERE created_at BETWEEN '{0}' AND '{1}'".format(from_date, to_date)
        res = db.select_from_table("tbl_users", ["username", "firstname", "lastname", "created_at"], condition)
        if res:
          for r in res:
            r['created_at'] = str(r['created_at'])
          print(res)
          return res
        else:
          return False
      except Exception as e:
        print(e)
        return False


  def create_logs(self, firstname, lastname, dept):
    '''
    Function to create logs
    Params: [firstname, lastname, dept]
    return [List]
    '''
    db = dbFunctions.db_class()
    table = "tbl_logs"
    fields = {"firstname": firstname, "lastname": lastname, "department": dept}
    try:
      res = db.insert_in_table(table, fields)
      print(res)
      if res:
        return True
      return False
    except Exception as e:
      raise e
      return False


  def load_logs(self):
    '''
    Function to fetch all logs from the database
    Params: [void]
    return [List]
    '''
    db = dbFunctions.db_class()
    table = "tbl_logs"
    try:
      res = db.select_from_table(table)
      print(res)
      if res:
        for r in res:
          r['login_time'] = str(r['login_time'])
          r['logout_time'] = str(r['logout_time'])
        print(res)
        return res
      else:
        return False
    except Exception as e:
      print(e)
      return False

