#Import mysql library
import mysql.connector

class db_class:
	"""
	This class manages all database connections
	"""

	cnx = None #DB connection variable
	cursor = None #DB cursor variable

	#Constructor
	def __init__(self, database_name='tanda_db'):
		"""
		Creates a database connection instance
		"""
		try:
			self.cnx = mysql.connector.connect(user='root',password="password", database=database_name)
			self.cursor = self.cnx.cursor(buffered=True, dictionary=True)
			pass
		except Exception as e:
			print("Error: DB Connection error")
			raise e

	def getInstance(self):
		"""
		Returns created DB connection instance
		"""
		if self.cnx != None:
			return self.cnx

	def getInstanceCursor(self):
		"""
		Returns created DB connection cursor
		"""
		if self.cursor != None:
			return self.cursor
		pass

	def closeInstanceConnection(self):
		"""
		Returns created DB connection cursor
		"""
		if self.cnx != None:
			return self.cnx.close()
		pass


	def insert_in_table(self, table, fields):
		"""
		This function insert data into a mysql database.
		Parameters: table  => String(Name of the table)
					fields => Dictionary(key is table field name, value is the value to insert)
		Returns: Boolean(True for success and False for failure)
		"""
		keys = "" #contains table fields
		values = "" #contains table values
		print(fields)
		try:
			for key, value in fields.items():
				keys += str(key) +","
				if value == "NOW()":
					values += str(value) + ","
				else:
					values += "'" + str(value) + "'" + ","

			#remove the commars from the end of the variables
			keys = keys[:-1]
			values = values[:-1]

			print(keys)
			print(values)
			#form Query
			query = "INSERT INTO " + table +"("+ keys + ") " + "VALUES(" + values + ");"
			print(query)
			#Excute query and process results
			self.cursor.execute(query)
			self.cnx.commit()
			#return boolen true on success
			return True
		except Exception as e:
			raise e
			return False



	def select_from_table(self, table, fields=["*"], condition="WHERE 1"):
		"""
		This function selects data form a mysql database.
		Parameters: table     => String(Name of the table)
					fields    => List(Value is table field name)
					condition => String(condition for selection)
		Returns: dictionary(Contains query results)
		"""
		keys = ""
		try:
			for value in fields:
				keys += str(value) +","
			#remove the commars from the end of the variables
			keys = keys[:-1]

			print(keys)
			#form Query
			query = "SELECT " + keys + " FROM " + table + " " + condition + ";"
			print(query)
			#Excute query and process results
			self.cursor.execute(query)
			res = self.cursor.fetchall()
			#return boolen true on success
			return res
		except Exception as e:
			raise e
			return False


	def joint_select(self, table_main, join_tables=[], fields=["*"], on_condition=[] ,gen_condition="WHERE 1"):
		"""
		This function selects data form multiple tables using join a mysql database.
		Parameters: table_main => String(Main table name)
					join_table => list(Name of the table) Eg.[table1, table2]
					fields     => List(Value is table field name) Eg.["table1.field1", "table2.field1"]
					on_condition  => String(condition for selection) Eg.["table_main.id=table2.id", "table1.id=table2.id"]
					gen_condition  => String(condition for selection)
		Returns: dictionary(Contains query results)
		"""

		if len(join_tables) == len(on_condition):
			keys = ""
			joins = ""
			tb_len = len(join_tables)
			try:
				#Loop is for the fields to select
				for value in fields:
					keys += str(value) +","
				#remove the commars from the end of the variables
				keys = keys[:-1]

				#Loop is for the fields to select
				for x in range(tb_len):
					joins +=  "INNER JOIN " + join_tables[x] + " ON " + on_condition[x] + " "
					pass

				print(keys)
				print(joins)
				#form Query
				query = "SELECT " + keys + " FROM " + table_main + " " + joins + " " + gen_condition + ";"
				print(query)
				#Excute query and process results
				self.cursor.execute(query)
				res = self.cursor.fetchall()
				#return boolen true on success
				return res
			except Exception as e:
				raise e
				return False
		else:
			print("join_tables and on_condition should be of the same length")
			return {"code":"02", "msg":"join_tables and on_condition should be of the same length"}



	def update_table(self, table, fields, condition="WHERE 1"):
		"""
		This function update data into a mysql database.
		Parameters: table => String(Name of the table)
					fields => Dictionary(key is table field name, value is the value to insert)
					condition => String(condition for selection)
		Returns: Boolean(True for success and False for failure)
		"""
		set_value = "" #contains table values
		try:
			for key, value in fields.items():
				if value == "NOW()":
					set_value += str(key) +"=" + str(value) + ","
				else:
					set_value += str(key) +"=" + "'" +str(value) + "'" + ","
			#remove the commars from the end of the variables
			set_value = set_value[:-1]

			print(set_value)
			#form Query
			query = "UPDATE " + table +" SET " + set_value + " " + condition + ";"
			print(query)
			#Excute query and process results
			self.cursor.execute(query)
			self.cnx.commit()
			#return boolen true on success
			return True
		except Exception as e:
			raise e
			return False


	def delete_from_table(self, table, condition="WHERE 1"):
		"""
		This function selects data form a mysql database.
		Parameters: table     => String(Name of the table)
					condition => String(condition for selection)
					fields    => List(Value is table field name)
		Returns: dictionary(Contains query results)
		"""
		try:
			#form Query
			query = "DELETE FROM " + table + " " + condition + ";"
			print(query)
			#Excute query and process results
			self.cursor.execute(query)
			self.cnx.commit()
			#return boolen true on success
			return True
		except Exception as e:
			raise e
			return False


	def select_count_from_table(self, table, fields=["*"], condition="WHERE 1"):
		"""
		This function selects data form a mysql database.
		Parameters: table     => String(Name of the table)
					fields    => List(Value is table field name)
					condition => String(condition for selection)
		Returns: dictionary(Contains query results)
		"""
		keys = ""
		try:
			for value in fields:
				keys += str(value) +","
			#remove the commars from the end of the variables
			keys = keys[:-1]

			print(keys)
			#form Query
			query = "SELECT COUNT("+keys+")"+ " FROM " + table + " " + condition + ";"
			print(query)
			#Excute query and process results
			self.cursor.execute(query)
			res = self.cursor.fetchall()
			#return boolen true on success
			# print(res[0]['COUNT(*)'])
			return res[0]['COUNT(*)']
		except Exception as e:
			raise e
			return False


# if __name__ == '__main__':
		# db = db_class("cedimerchant")

		'''
		for insert_in_table
		'''
	#fields = {"type":"200", "name":"USER", "details":"Portal User"}
	#table = "lst_privileges_type"
	#result = db.insert_in_table(table, fields)

		'''
		for select_from_table
		'''
	#table = "lst_privileges_type"
	#fields = ["type", "name"]
	#condition = "WHERE type=300"
	#result = db.select_from_table(table) #returns all from the table
	#result = db.select_from_table(table, fields, condition)

		'''
		for select_count_from_table
		'''
		# table = "tbl_merchant"
	#fields = ["type", "name"]
	#condition = "WHERE type=300"
		# result = db.select_count_from_table(table) #returns all from the table
		# print(result)
		# result = db.select_from_table(table, fields, condition)
		'''
		for joint_select
		'''
	#fields = ['topics_feeds.*', 'topics.*']
	#condition = "WHERE 1"
	#table = "topics_feeds"
	#jtables = ['topics', 'feed_delivery']
	#oncond = ['topics_feeds.t_id = topics.id', 'delivery.f_id = topics_feeds.fid']
	#result = db.joint_select(table,jtables,fields,oncond,condition)


		'''
		for update_table
		'''
	#table = "lst_privileges_type"
	#fields = {"type":"300"}
	#condition = "WHERE name='USER'"
	#result = db.update_table(table, fields, condition)


		'''
		for delete
		'''
	#result = db.delete_from_table("lst_privileges_type", "WHERE type=200")

	#db.closeInstanceConnection()
	#print(result)
