from flask import request, session, redirect, url_for, g, abort, render_template, flash, make_response, Flask
from flask.ext import excel
from passlib.hash import sha256_crypt
from flask.ext.cors import CORS
from modules import appFunctions
from modules import decorators
import json
import datetime
import pyexcel_xls

#import flask app
app = Flask(__name__)
CORS(app)
app.config.from_object('config')
#Propagate exceptions on the server[PRODUCTION]
app.config['PROPAGATE_EXCEPTIONS'] = True
#Creating instance of appFunctions
app_func = appFunctions.app_calls()
decorate = decorators.Validates()


@app.route('/', methods=['GET', 'POST'])
def search():
  if request.method == "POST":
    search = request.form['q']
    if search:
      emp = app_func.fetch_employee(search)[0]
      print(emp)
      res = app_func.create_logs(emp['firstname'], emp['lastname'], emp['department'])
      if res:
        flash("Log successfully added for {0}".format(search))
        return render_template('search.html', title="Search")
  return render_template('search.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
  error = ""
  if request.method == "POST":
    username = request.form['username']
    password = request.form['password']
    if username !='' and password !='':
      log = app_func.login(username)
      print(log)
      if log:
        session['username']  = log['username']
        session['firstname'] = log['firstname']
        session['lastname']  = log['lastname']
        if sha256_crypt.verify(password, log['password']):
          session['logged_in'] = True
          return redirect(url_for('home'))
        error = "Wrong username or password"
        return render_template('login.html', error=error)
    error = "Invalid credentials"
    return render_template('login.html', error=error)
  return render_template('login.html')


#logout
@app.route('/logout')
def logout():
  session.pop('logged_in', None)
  # flash('You were logged out')
  return redirect(url_for('login'))


# MAIN dashboard
@app.route('/home')
@decorate.login_required
def home():
  return render_template('home.html', title="home")


@app.route('/logs')
def manage_logs():
  if not session.get('logged_in'):
    abort(401)
  else:
    res = app_func.load_logs()
    if res:
      return render_template('logs.html', title="logs", data=res)
    return render_template('logs.html', title="logs")


@app.route('/add_admin', methods=['GET', 'POST'])
def add_admin():
  if not session.get('logged_in'):
    abort(401)
  elif request.method == "POST":
    error = ""
    if request.form['admin_username'] and request.form['admin_firstname'] and request.form['admin_lastname'] and request.form['admin_password']:
      if request.form['admin_password'] == request.form['admin_confirm_password']:
        print(request.form)
        res = app_func.create_admin(request.form['admin_username'], request.form['admin_firstname'], request.form['admin_lastname'], request.form['admin_password'])
        if res:
          flash("New administrator successfully saved")
          return render_template('add_admin.html', title="Create Admin")
        else:
          error = "An error occured while attempting to save."
          return render_template('add_admin.html', error=error, title="Create Admin")
      else:
        error = "Password does not match confirmation"
        return render_template('add_admin.html', error=error, title="Create Admin")
  else:
    return render_template('add_admin.html', title="Create Admin")


@app.route('/administrators')
def view_admin():
  if not session.get('logged_in'):
    abort(401)
  else:
    res = app_func.load_admin()
    if res:
      return render_template('admins.html', title="View_Admin", data=res)
    return render_template('admins.html', title="View_Admin")


@app.route('/add_employee', methods=['GET', 'POST'])
def add_user():
  if not session.get('logged_in'):
    abort(401)
  elif request.method == "POST":
    error = ""
    if request.form['firstname'] and request.form['lastname'] and request.form['department'] and request.form['email']:
      print(request.form)
      res = app_func.create_employee(request.form['firstname'], request.form['lastname'], request.form['department'], request.form['email'])
      if res:
        flash("New employee successfully saved")
        return render_template('add_user.html', title="Create Employee" )
      else:
        error = "An error occured while attempting to save."
        return render_template('add_user.html', error=error, title="Create Employee" )
    else:
      error = "Empty parameter experience"
      return render_template('add_user.html', error=error, title="Create Employee" )
  else:
    return render_template('add_user.html', title="Create Employee")


@app.route('/employees')
def users():
  if not session.get('logged_in'):
    abort(401)
  else:
    res = app_func.load_employees()
    if res:
      return render_template('users.html', title="View_Employee", data=res)
    return render_template('users.html', title="View_Employee")

@app.route('/emp')
def json_emp():
  if request.method == "GET":
    emp = app_func.load_employees()
    if emp:
      return json.dumps({'code': "00", 'msg': emp})
    return json.dumps({'code': "01", 'msg': 'No data found'})
  return json.dumps({'code': "01", 'msg':'Wrong request method used.'})


@app.route('/download', methods=['GET', 'POST'])
def generate_reports():
  if not session.get('logged_in'):
    abort(401)
  else:
    if request.method == "POST":
      print(request.form)
      if request.form['from_date'] and request.form['to_date'] and request.form['report_type']:
        response = app_func.download_reports(request.form['from_date'], request.form['to_date'], request.form['report_type'])
        print(response)
        r = excel.make_response_from_records(response, 'xls')
        make_response(r)
        # print(r)
        r.headers['Content-Disposition'] = "attachment; filename={0}.xls".format(request.form['report_type'])
        r.headers['Content-Type'] = "text/csv"
        return r
      else:
        error = "Input fields cannot be empty"
        return render_template('reports.html', title="generate_report", error=error)
    return render_template('reports.html', title="generate_report")


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html")

@app.errorhandler(500)
def server_error(e):
    return render_template("500.html")

if __name__ == '__main__':
  app.run(debug=True, port=5055)
